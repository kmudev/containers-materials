# Brief history of containers 1979-2020

   ##### Unix V7 in 1979  
   - introduction of the 'chroot' system call (It's an operation that changes the apparent root directory for the current running process and its children. A program that is run in such a modified environment cannot name (and therefore normally cannot access) files outside the designated directory tree). Chroot was added to BSD in 1982. The first isolation mechanism (segregation of file access for processes)

   ##### FreeBSD Jails in 2000 
   - allows administrators to partition a FreeBSD system into several independent, smaller systems – called “jails” – with the ability to assign an IP address for each system and configuration (separation between own and customers' services, running on the same machine)

   ##### Linux VServer in 2001 
   - jail mechanism for Linux (file systems, network addresses, memory). Last stable patch was in 2006.

   ##### Borg System around 2003-2004 
   - created by Google. The great-grandfather of Kubernetes. Ran hundreds of thousands of jobs, from many thousands of different applications, across many clusters, each with up to tens of thousands of machines

   ##### Solaris contianers in 2004 
   - system resource controls and boundary separation provided by "zones" that have snapshots and cloning coming from ZFS

   ##### Open VZ in 2005 
   - operating system-level virtualization technology for Linux. Didn't make it to the kernel officially.

   ##### Process Containers in 2006 
   - Google made them. Designed for limiting, accounting and isolating resource usage (CPU, memory, disk I/O, network) of a collection of processes. It was renamed “Control Groups (cgroups)” a year later and eventually merged to Linux kernel 2.6.24.
   
   ##### LXC in 2008 
   - the first, most complete implementation of Linux container manager. Using cgroups and Linux namespaces - it works on a single Linux kernel without requiring any patches.
   
   ##### Warden in 2011 
   - by CloudFoundry, using LXC under the hood, later replaced by own implementation. Running as a daemon and providing an API for container management.

   ##### LMCTFY (Let me contain that for you) in 2013 
   - open-source version of Google's container stack. Applications can be made “container aware,” creating and managing their own subcontainers. Deployments stopped in 2015 when Google started contributing to libcontainer and it's not part of Open Container Foundation.

   ##### Docker in 2013 
   - container popularity exploded. Like Warden, used LXC early, but they created a whole ecosystem.

   ##### Kubernetes in 2014 
   - it's the third "container management" project by Google. Released as open-sourced version of Borg.

   ##### 2015 
   - Google partnered with the Linux Foundation to form the Cloud Native Computing Foundation (CNCF). The CNFC aims to build sustainable ecosystems and to foster a community around a constellation of high-quality projects that orchestrate containers as part of a microservices architecture. Kubernetes is donated to CNFC.

   ##### 2016-2017 
   - Kubernetes goes mainstream. Containers become more complex. Security needs for containerized setups start to skyrocket. Mass adoption of containers in the Enterprise.

   ##### 2018-2020 
   - cloud integrations (GKE, AWS, Azure, IBM Cloud), going serverless, Kubernetes dominates, Docker out of the big business (Swarm and Enterprise sold), VMWare starts to compete


##### More reads:
 - Original article: https://blog.aquasec.com/a-brief-history-of-containers-from-1970s-chroot-to-docker-2016
 - Borg: https://storage.googleapis.com/pub-tools-public-publication-data/pdf/43438.pdf
 - Borg, Omega and Kubernetes: https://storage.googleapis.com/pub-tools-public-publication-data/pdf/44843.pdf
 - Origin of Kubernetes: https://cloud.google.com/blog/products/gcp/from-google-to-the-world-the-kubernetes-origin-story
 - Timeline: https://blog.risingstack.com/the-history-of-kubernetes/