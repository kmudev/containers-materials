# Let's start from somewhere: LXC

### What is LXC (LinuX Containers)?
Lightweight Linux OS-virtualization technology, that allows creation of containers and is written in C.
   - they use Linux Virtual Environments (cgroups + chroot and company)
   - works great on embedded systems (and such with low resource)
   - LXC is a much more VM-like container runtime 
   - it boots an OS (including an init system (systemd))
   - managed by virtual consoles
   - Docker was based on it in it's early stages


### How Docker was born?

Before version 1.11, the implementation of Docker was a monolithic daemon that contained:
   - downloading container images
   - launching container processes
   - exposing a remote API
   - log collection daemon 
   - one centralized process running as root

LXC was the default runtime for Docker up until 2014. There was no privilege separation, it was hard to integrate it with init systems, so the monolith was split...<br/>
**Good read:** [Docker vs. LXC](https://www.upguard.com/articles/docker-vs-lxc)


> “We are excited to introduce Docker Engine 1.11, our first release built on runC and containerd. With this release, Docker is the first to ship a runtime based on OCI* technology, demonstrating the progress the team has made since donating our industry-standard container format and runtime under the Linux Foundation in June of 2015”

![Containerd is born](./pic/birth_of_containerd.PNG)

And so Docker made ***libcontainer*** (which is now called ***runC*** and written in Go). It took over.

### What is runc?
[***Check out the official repo, it has all you need***](https://github.com/opencontainers/runc) 
 - runC is a CLI tool for spawning and running containers according to the OCI Specification. It's not involved that much in the container lifecycle. It supports wider range of isolation technologies and abstracts away the complex stuff (syscalls and etc.) 

### What is OCI Specification?
 - Standardization of the container technologies. Covers different platforms and all the "rules" for implementing containers.
The following repositories contain enough information:
   - https://github.com/opencontainers/runtime-spec
   - https://github.com/opencontainers/runtime-spec/blob/master/spec.md

### What is containerd?

[***The official repo (specifically the design)***](https://github.com/containerd/containerd/tree/master/designc)
  - "a daemon that controls runC" or more like "an API facade to runC"
  > "It is available as a daemon for Linux and Windows, which can manage the complete container lifecycle of its host system: image transfer and storage, container execution and supervision, low-level storage and network attachments, etc."
  - Works with high level concepts such as snapshots, containers and etc. So it's a container runtime (can also be plugged to Kubernetes, as well as CRI-O*). It's OCI compliant.

[***An interesting topic comparing containerd and runC***](https://stackoverflow.com/questions/41645665/how-containerd-compares-to-runc)

### What containerd takes care of
![Containerd](./pic/containerd_architecture.png)


### Docker's interactions, components and architecture

![Docker interactions with libraries](./pic/docker_linux.png)

![Docker components](./pic/docker-components.png)

![Docker architecture](./pic/docker_architecture.png)

### So how does the workflow look like?

1. A user runs commands from ***Docker-CLI*** (docker run, docker build...)
2. ***Docker-CLI*** talks to the Docker daemon (***dockerd***)
3. Docker daemon(***dockerd***) listens for requests (via REST API) and manages the lifecycle of the container via ***containerd*** which it contacts
4. ***containerd*** takes the request and starts a container through ***runC*** and does all the container life-cycle operations within the host.
5. ***runC*** does the "dirty work" and takes care of all "lower level" tasks (namespaces, cgroups and other mechanisms)




